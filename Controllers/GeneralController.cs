using System;
using Microsoft.AspNetCore.Mvc;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class GeneralController : ControllerBase
    {
        private IGeneralModel _generalModel;
        public GeneralController(IGeneralModel generalModel)
        {
            _generalModel = generalModel;
        }

        /// <summary>
        /// Busca datos de conceptos
        /// </summary>
        /// <param name="Flag">Flag modo de búsqueda</param>
        /// <param name="Prefix">Código de concepto</param>
        /// <param name="Correlative">Correlativo de concepto</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpGet("BuscarConcepto/{Flag:int}/{Prefix:int}/{Correlative:int}")]
        public IActionResult getDataConcept(int Flag, int Prefix, int Correlative)
        {
            try
            {
                var response = _generalModel.getDataConcept(Flag, Prefix, Correlative);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }            

        /// <summary>
        /// Busca datos de parámetros
        /// </summary>
        /// <param name="Flag">Flag modo de búsqueda</param>
        /// <param name="Prefix">Código de parámetro</param>
        /// <param name="Correlative">Correlativo de parámetro</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpGet("BuscarParametro/{Flag:int}/{Prefix:int}/{Correlative:int}")]
        public IActionResult getDataParameter(int Flag, int Prefix, int Correlative)
        {
            try
            {
                var response = _generalModel.getDataParameter(Flag, Prefix, Correlative);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Busca datos de presentaciones de producto
        /// </summary>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpGet("BuscarPresentacionesProducto")]
        public IActionResult getProductPresentations()
        {
            try
            {
                var response = _generalModel.getProductPresentations();

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Busca datos de ubigeo
        /// </summary>
        /// <param name="Flag">Flag modo de búsqueda</param>
        /// <param name="DepartmentId">Código de departamento</param>
        /// <param name="ProvinceId">Código de provincia</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpGet("BuscarUbigeo/{Flag:int}/{DepartmentId:int}/{ProvinceId:int}")]
        public IActionResult getLocation(int Flag, int DepartmentId, int ProvinceId)
        {
            try
            {
                var response = _generalModel.getLocation(Flag, DepartmentId, ProvinceId);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        } 
    }
}