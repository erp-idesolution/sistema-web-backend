﻿using System;
using Microsoft.AspNetCore.Mvc;
using WebApi.Helpers;
using WebApi.Models;
using WebApi.Entities;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private IUserModel _userModel;
        public UserController(IUserModel userModel)
        {
            _userModel = userModel;
        }
        /// <summary>
        /// Valida credenciales de inico de sesion de usuario
        /// </summary>
        /// <param name="loginRequest">Objeto con credenciales a validar</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>        
        [HttpPost("ValidarCredenciales")]
        public IActionResult ValidateLogin(LoginRequest loginRequest)
        {
            try
            {
                var response = _userModel.ValidateLogin(loginRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }
    }
}