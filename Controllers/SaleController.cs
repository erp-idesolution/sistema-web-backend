using System;
using Microsoft.AspNetCore.Mvc;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {
        private ISaleModel _saleModel;
        public SaleController(ISaleModel saleModel)
        {
            _saleModel = saleModel;
        }
        
        /// <summary>
        /// Registra venta de cliente
        /// </summary>
        /// <param name="addUpdSaleRequest">Objeto de venta a registrar</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>        
        [Authorize]
        [HttpPost("RegistrarVenta")]
        public IActionResult AddSale(AddUpdSaleRequest addUpdSaleRequest)
        {
            try
            {
                var response = _saleModel.AddSale(addUpdSaleRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Modifica detalle de venta de cliente
        /// </summary>
        /// <param name="addUpdSaleRequest">Objeto de venta a modificar</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>        
        [Authorize]
        [HttpPut("EditarVenta")]
        public IActionResult UpdateSale(AddUpdSaleRequest addUpdSaleRequest)
        {
            try
            {
                var response = _saleModel.UpdateSale(addUpdSaleRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Busca todas las ventas registradas
        /// </summary>
        /// <param name="listSalesRequest">Objeto con paramétros de búsqueda</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response> 
        [Authorize]
        [HttpPost("BuscarVentas")]
        public IActionResult GetSales(ListSalesRequest listSalesRequest)
        {
            try
            {
                var response = _saleModel.GetSales(listSalesRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }        
    }
}