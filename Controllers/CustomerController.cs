using System;
using Microsoft.AspNetCore.Mvc;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private ICustomerModel _customerModel;
        public CustomerController(ICustomerModel customerModel)
        {
            _customerModel = customerModel;
        }

        /// <summary>
        /// Registra cliente
        /// </summary>
        /// <param name="addUpdCustomerRequest">Objeto de cliente a registrar</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpPost("RegistrarCliente")]
        public IActionResult AddCustomer(AddUpdCustomerRequest addUpdCustomerRequest)
        {
            try
            {
                var response = _customerModel.AddCustomer(addUpdCustomerRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Modifica registro de cliente
        /// </summary>
        /// <param name="addUpdCustomerRequest">Objeto de cliente a modificar</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpPut("EditarCliente")]
        public IActionResult UpdateCustomer(AddUpdCustomerRequest addUpdCustomerRequest)
        {
            try
            {
                var response = _customerModel.UpdateCustomer(addUpdCustomerRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Elimina registro de cliente
        /// </summary>
        /// <param name="CustomerId">Código de cliente</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpDelete("EliminarCliente/{CustomerId:int}")]
        public IActionResult DeleteCustomer(int CustomerId)
        {
            try
            {
                var response = _customerModel.DeleteCustomer(CustomerId);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Busca todos los clientes registrados
        /// </summary>
        /// <param name="listCustomersRequest">Objeto con paramétros de búsqueda</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpPost("BuscarClientes")]
        public IActionResult GetCustomers(ListCustomersRequest listCustomersRequest)
        {
            try
            {
                var response = _customerModel.GetCustomers(listCustomersRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }

        /// <summary>
        /// Busca datos de cliente en caso este registrado
        /// </summary>
        /// <param name="DocumentNumber">Numero de documento de cliente</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpGet("BuscarCliente/{DocumentNumber}")]
        public IActionResult GetDataCustomer(string DocumentNumber)
        {
            try
            {
                var response = _customerModel.getDataCustomer(DocumentNumber);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }
    }
}