using System;
using Microsoft.AspNetCore.Mvc;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private IProductModel _productModel;
        public ProductController(IProductModel productModel)
        {
            _productModel = productModel;
        }
        /// <summary>
        /// Busca datos de producto en caso este registrado
        /// </summary>
        /// <param name="SKU">SKU de producto</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpGet("BuscarProducto/{SKU}")]
        public IActionResult GetDataProduct(string SKU)
        {
            try
            {
                var response = _productModel.getDataProduct(SKU);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }
    }
}