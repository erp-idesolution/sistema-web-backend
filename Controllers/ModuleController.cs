using System;
using Microsoft.AspNetCore.Mvc;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class ModuleController : ControllerBase
    {
        private IModuleModel _moduleModel;
        public ModuleController(IModuleModel moduleModel)
        {
            _moduleModel = moduleModel;
        }
        /// <summary>
        /// Retorna codigos y nombres de modulos y submodulos de perfil de usuario
        /// </summary>
        /// <param name="ProfileId">Identificador de perfil de usuario</param>
        /// <param name="UserId">Identificador de usuario</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>
        [Authorize]
        [HttpGet("AccesosModulos/{ProfileId:int}/{UserId:int}")]
        public IActionResult GetUserModules(int ProfileId, int UserId)
        {
            try
            {
                var response = _moduleModel.GetUserModules(ProfileId, UserId);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }
    }
}