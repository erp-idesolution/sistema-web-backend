using System;
using Microsoft.AspNetCore.Mvc;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private IPaymentModel _paymentModel;
        public PaymentController(IPaymentModel paymentModel)
        {
            _paymentModel = paymentModel;
        }
        
        /// <summary>
        /// Registra pago de venta
        /// </summary>
        /// <param name="addPaymentRequest">Objeto de pago a registrar</param>
        /// <response code="401">No Autorizado. No se ha indicado o es incorrecto el Token JWT de acceso.</response>              
        /// <response code="200">OK. Devuelve el objeto solicitado.</response>        
        /// <response code="404">No Encontrado. No se ha encontrado el objeto solicitado.</response>        
        [Authorize]
        [HttpPost("RegistrarPago")]
        public IActionResult AddPayment(AddPaymentRequest addPaymentRequest)
        {
            try
            {
                var response = _paymentModel.AddPayment(addPaymentRequest);

                if (response.codigo != 200) return BadRequest(response);

                return Ok(response);   
            }
            catch (Exception)
            {
                return BadRequest(new ResponseHeader()
                {
                    codigo = 400,
                    descripcion = "Error",
                    mensaje = "Ocurrió un error"
                });
            }
        }
    }
}