using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface ICustomerModel
    {
        ResponseHeader getDataCustomer(string documentNumber);
        ResponseHeader AddCustomer(AddUpdCustomerRequest listCustomersRequest);
        ResponseHeader UpdateCustomer(AddUpdCustomerRequest addUpdCustomerRequest);
        ResponseHeader DeleteCustomer(int CustomerId);
        ResponseHeader GetCustomers(ListCustomersRequest listCustomersRequest);
    }
    public class CustomerModel : ICustomerModel
    {
        private DataContext _context;
        GeneralModel generalModel;
        public CustomerModel(DataContext context)
        {
            _context = context;
        }
        public ResponseHeader AddCustomer(AddUpdCustomerRequest addUpdCustomerRequest)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_registrar_cliente";
                string xmlCustomer = ApiHelper.SerializeObject(addUpdCustomerRequest);
                cmd.Parameters.Add(new SqlParameter("@Cliente", xmlCustomer));

                var codigoRetorno = cmd.Parameters.Add("@Codigo", SqlDbType.Int); 
                codigoRetorno.Direction = ParameterDirection.Output;

                var mensajeRetorno = cmd.Parameters.Add("@Mensaje", SqlDbType.NVarChar, 500); 
                mensajeRetorno.Direction = ParameterDirection.Output;

                ApiHelper.OpenConnection(sqlCon);

                cmd.ExecuteNonQuery();
                int codigo = (int) codigoRetorno.Value;
                string mensaje = mensajeRetorno.Value.ToString();

                ApiHelper.CloseConnection(sqlCon);

                if(codigo > 0) {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = (new Response(){
                        Codigo = codigo,
                        Mensaje = mensaje
                    });
                } else {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "Ocurrió un error";
                    
                    generalModel.saveControlError(Constant.IdApiRegistrarCliente, addUpdCustomerRequest, "AddCustomer", mensaje, "pa_registrar_cliente", 0, "0");
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiRegistrarCliente, addUpdCustomerRequest, "AddCustomer", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader UpdateCustomer(AddUpdCustomerRequest addUpdCustomerRequest)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_editar_cliente";
                string xmlCustomer = ApiHelper.SerializeObject(addUpdCustomerRequest);
                cmd.Parameters.Add(new SqlParameter("@Cliente", xmlCustomer));

                var codigoRetorno = cmd.Parameters.Add("@Codigo", SqlDbType.Int); 
                codigoRetorno.Direction = ParameterDirection.Output;

                var mensajeRetorno = cmd.Parameters.Add("@Mensaje", SqlDbType.NVarChar, 500); 
                mensajeRetorno.Direction = ParameterDirection.Output;

                ApiHelper.OpenConnection(sqlCon);

                cmd.ExecuteNonQuery();
                int codigo = (int) codigoRetorno.Value;
                string mensaje = mensajeRetorno.Value.ToString();

                ApiHelper.CloseConnection(sqlCon);

                if(codigo > 0) {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = (new Response(){
                        Codigo = codigo,
                        Mensaje = mensaje
                    });
                } else {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "Ocurrió un error";
                    
                    generalModel.saveControlError(Constant.IdApiEditarCliente, addUpdCustomerRequest, "UpdateCustomer", mensaje, "pa_editar_cliente", 0, "0");
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiEditarCliente, addUpdCustomerRequest, "UpdateCustomer", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader DeleteCustomer(int CustomerId)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_eliminar_cliente";
                cmd.Parameters.Add(new SqlParameter("@CodigoCliente", CustomerId));

                var codigoRetorno = cmd.Parameters.Add("@Codigo", SqlDbType.Int); 
                codigoRetorno.Direction = ParameterDirection.Output;

                var mensajeRetorno = cmd.Parameters.Add("@Mensaje", SqlDbType.NVarChar, 500); 
                mensajeRetorno.Direction = ParameterDirection.Output;

                ApiHelper.OpenConnection(sqlCon);

                cmd.ExecuteNonQuery();
                int codigo = (int) codigoRetorno.Value;
                string mensaje = mensajeRetorno.Value.ToString();

                ApiHelper.CloseConnection(sqlCon);

                if(codigo > 0) {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = (new Response(){
                        Codigo = codigo,
                        Mensaje = mensaje
                    });
                } else {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "Ocurrió un error";
                    
                    generalModel.saveControlError(Constant.IdApiEliminarrCliente, CustomerId, "DeleteCustomer", mensaje, "pa_eliminar_cliente", 0, "0");
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiEliminarrCliente, CustomerId, "DeleteCustomer", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader GetCustomers(ListCustomersRequest listCustomersRequest)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);            

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_clientes";

                cmd.Parameters.Add(new SqlParameter("@NumeroDocumento", listCustomersRequest.NumeroDocumento));
                cmd.Parameters.Add(new SqlParameter("@DatosCliente", listCustomersRequest.DatosCliente));
                cmd.Parameters.Add(new SqlParameter("@TipoPersona", listCustomersRequest.TipoPersona));

                ApiHelper.OpenConnection(sqlCon);

                var customers = new List<ListCustomersResponse>();
           
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var res = new ListCustomersResponse{
                            CodigoCliente = (int) reader["idPersona"],
                            TipoPersona = (int) reader["tipoPersona"],
                            DescripcionTipoPersona = reader["descripcionTipoPersona"].ToString(),
                            Nombres = reader["nombre"].ToString() + " " + reader["apellidoPaterno"].ToString() + " " + reader["apellidoMaterno"].ToString(),
                            TipoDocumento = (int) reader["tipoDocumento"],
                            NumeroDocumento = reader["numDocumento"].ToString(),
                            NumeroContacto = reader["numeroContacto"].ToString(),
                            Correo = reader["email"].ToString(),
                            Direccion = reader["direccion"].ToString(),
                            Departamento = reader["NombreDepartamento"].ToString(),
                            Provincia = reader["NombreProvincia"].ToString(),
                            Distrito = reader["NombreDistrito"].ToString()
                        };
                        if (reader["fechaNacimiento"] != DBNull.Value) res.FechaNacimiento = Convert.ToDateTime(reader["fechaNacimiento"].ToString()).ToString("dd/MM/yyyy");
                        customers.Add(res);
                    }
                }

                ApiHelper.CloseConnection(sqlCon);

                if(customers.Count == 0){
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontraron clientes";
                } else {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = customers;
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiBuscarClientes, listCustomersRequest, "GetCustomers", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader getDataCustomer(string documentNumber)
        {
            ResponseHeader response = new ResponseHeader();
            Person person = null;

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_datos_cliente";
                cmd.Parameters.Add(new SqlParameter("@numero_documento", documentNumber));

                ApiHelper.OpenConnection(sqlCon);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        person = new Person();
                        person.TipoDocumento = (int) reader["tipoDocumento"];
                        person.Nombres = reader["nombre"].ToString();
                        person.ApellidoPaterno = reader["apellidoPaterno"].ToString();
                        person.ApellidoMaterno = reader["apellidoMaterno"].ToString();
                    }
                }
                
                ApiHelper.CloseConnection(sqlCon);

                if (person == null) {
                    response.codigo = 404;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontró cliente";
                } else {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = person;
                }
                
            }
            catch (Exception e)
            {
                generalModel = new GeneralModel(_context);
                generalModel.saveControlError(Constant.IdApiBuscarCliente, documentNumber, "getDataCustomer", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
    }
}