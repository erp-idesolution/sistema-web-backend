using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface IPaymentModel
    {
        ResponseHeader AddPayment(AddPaymentRequest addPaymentRequest);
    }
    public class PaymentModel : IPaymentModel
    {
        private DataContext _context;
        GeneralModel generalModel;
        public PaymentModel(DataContext context)
        {
            _context = context;
        }
        public ResponseHeader AddPayment(AddPaymentRequest addPaymentRequest)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_registrar_pago";
                string xmlPayment = ApiHelper.SerializeObject(addPaymentRequest);
                cmd.Parameters.Add(new SqlParameter("@Pago", xmlPayment));

                var codigoRetorno = cmd.Parameters.Add("@Codigo", SqlDbType.Int); 
                codigoRetorno.Direction = ParameterDirection.Output;

                var mensajeRetorno = cmd.Parameters.Add("@Mensaje", SqlDbType.NVarChar, 500); 
                mensajeRetorno.Direction = ParameterDirection.Output;

                var serie = cmd.Parameters.Add("@Serie", SqlDbType.VarChar, 10); 
                serie.Direction = ParameterDirection.Output;

                var correlativo = cmd.Parameters.Add("@Correlativo", SqlDbType.Int); 
                correlativo.Direction = ParameterDirection.Output;

                ApiHelper.OpenConnection(sqlCon);

                cmd.ExecuteNonQuery();

                int codigo = (int) codigoRetorno.Value;
                string mensaje = mensajeRetorno.Value.ToString();
                string seriePago = serie.Value.ToString();
                int correlativoPago = (int) correlativo.Value;

                ApiHelper.CloseConnection(sqlCon);

                if(codigo > 0) {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = (new AddPaymentResponse(){
                        CodigoPago = codigo,
                        Mensaje = mensaje,
                        SeriePago = seriePago,
                        CorrelativoPago = correlativoPago
                    });
                } else {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "Ocurrió un error";
                    
                    generalModel.saveControlError(Constant.IdApiRegistrarPago, addPaymentRequest, "AddPayment", mensaje, "pa_registrar_Pago", addPaymentRequest.CodigoSucursal, addPaymentRequest.CodigoUsuario.ToString());
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiRegistrarPago, addPaymentRequest, "AddPayment", e.Message, e.StackTrace, addPaymentRequest.CodigoSucursal, addPaymentRequest.CodigoUsuario.ToString());
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
    }
}