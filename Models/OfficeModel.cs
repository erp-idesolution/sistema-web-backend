using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface IOfficeModel
    {
        
    }
    public class OfficeModel : IOfficeModel
    {
        private DataContext _context;
        public OfficeModel(DataContext context)
        {
            _context = context;
        }
        public List<Office> getUserOffices(int _userId)
        {
            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_agencias_usuario";
                cmd.Parameters.Add(new SqlParameter("@codigo_usuario", _userId));

                ApiHelper.OpenConnection(sqlCon);

                var response = new List<Office>();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        response.Add(
                            new Office{
                                IdSucursal = (int) reader["IdSucursal_rel"],
                                RazonSocial = reader["razonSocial"].ToString()
                            }
                        );
                    }
                }

                ApiHelper.CloseConnection(sqlCon);
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}