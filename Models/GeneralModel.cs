using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.Json;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface IGeneralModel
    {
        ResponseHeader getDataConcept(int Flag, int Prefix, int Correlative);
        ResponseHeader getDataParameter(int Flag, int Prefix, int Correlative);
        ResponseHeader getProductPresentations();
        ResponseHeader getLocation(int Flag, int DepartmentId, int ProvinceId);
    }
    public class GeneralModel : IGeneralModel
    {
        private DataContext _context;
        public GeneralModel(DataContext context)
        {
            _context = context;
        }
        public void saveControlError(int _apiId, object _objectEntry, string _errorMethod, string _errorMessage, string _traceError, int _office, string _userDocument)
        {
            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();

                string _dataEntry = JsonSerializer.Serialize(_objectEntry);
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_registrar_control_error";
                cmd.Parameters.Add(new SqlParameter("@codigo_api", _apiId));
                cmd.Parameters.Add(new SqlParameter("@data_entrada", _dataEntry));
                cmd.Parameters.Add(new SqlParameter("@metodo_error", _errorMethod));
                cmd.Parameters.Add(new SqlParameter("@mensaje_error", _errorMessage));
                cmd.Parameters.Add(new SqlParameter("@traza_error", _traceError));
                cmd.Parameters.Add(new SqlParameter("@sucursal", _office));
                cmd.Parameters.Add(new SqlParameter("@documento_usuario", _userDocument));                

                var codigoRetorno = cmd.Parameters.Add("@codigo_retorno", SqlDbType.Int); 
                codigoRetorno.Direction = ParameterDirection.Output;
                var mensajeRetorno = cmd.Parameters.Add("@mensaje_retorno", SqlDbType.NVarChar, 50); 
                mensajeRetorno.Direction = ParameterDirection.Output;

                ApiHelper.OpenConnection(sqlCon);

                cmd.ExecuteNonQuery();
                var codigo = codigoRetorno.Value;
                var mensaje = mensajeRetorno.Value;

                ApiHelper.CloseConnection(sqlCon);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public ResponseHeader getDataConcept(int Flag, int Prefix, int Correlative)
        {
            ResponseHeader response = new ResponseHeader();

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_conceptos";
                cmd.Parameters.Add(new SqlParameter("@Flag", Flag));
                cmd.Parameters.Add(new SqlParameter("@Prefijo", Prefix));
                cmd.Parameters.Add(new SqlParameter("@Correlativo", Correlative));

                ApiHelper.OpenConnection(sqlCon);

                var concepts = new List<Concept>();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        concepts.Add(
                            new Concept{
                                Correlativo = (int) reader["correlativo"],
                                Descripcion = reader["descripcion"].ToString()
                            }
                        );
                    }
                }
                
                ApiHelper.CloseConnection(sqlCon);

                if (concepts.Count == 0) {
                    response.codigo = 404;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontraron conceptos";
                } else {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = concepts;
                }                
            }
            catch (Exception e)
            {
                string DataEntrada = "{ Flag: " + Flag + ", Prefijo: " + Prefix + ", Correlativo: " + Correlative + " }";
                saveControlError(Constant.IdApiBuscarConceptos, DataEntrada, "getDataConcept", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader getDataParameter(int Flag, int Prefix, int Correlative)
        {
            ResponseHeader response = new ResponseHeader();
            decimal IGV = 0;

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_parametros";
                cmd.Parameters.Add(new SqlParameter("@flag", Flag));
                cmd.Parameters.Add(new SqlParameter("@prefijo", Prefix));
                cmd.Parameters.Add(new SqlParameter("@correlativo", Correlative));

                ApiHelper.OpenConnection(sqlCon);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        IGV = (decimal) reader["IGV"];
                    }
                }
                
                ApiHelper.CloseConnection(sqlCon);

                if (IGV == 0) {
                    response.codigo = 404;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontró parámetro";
                } else {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = new GeneralParameter(){
                        IGV = IGV
                    };
                }                
            }
            catch (Exception e)
            {
                string DataEntrada = "{ Flag: " + Flag + ", Prefijo: " + Prefix + ", Correlativo: " + Correlative + " }";
                saveControlError(Constant.IdApiBuscarParametros, DataEntrada, "getDataParameter", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader getProductPresentations()                     
        {
            ResponseHeader response = new ResponseHeader();

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_presentaciones_producto";

                ApiHelper.OpenConnection(sqlCon);

                var presentations = new List<Presentation>();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        presentations.Add(
                            new Presentation {
                                IdPresentacion = (int) reader["IdPresentacion"],
                                DescripcionPresentacion = reader["DescripcionPresentacion"].ToString()
                            }
                        );                        
                    }
                }
                
                ApiHelper.CloseConnection(sqlCon);

                if (presentations.Count == 0) {
                    response.codigo = 404;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontraron presentaciones de producto";
                } else {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = presentations;
                }                
            }
            catch (Exception e)
            {
                saveControlError(Constant.IdApiBuscarPresentacionesProducto, "", "getProductPresentations", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader getLocation(int Flag, int DepartmentId, int ProvinceId)
        {
            ResponseHeader response = new ResponseHeader();

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_ubigeo";
                cmd.Parameters.Add(new SqlParameter("@Flag", Flag));
                cmd.Parameters.Add(new SqlParameter("@CodigoDepartamento", DepartmentId));
                cmd.Parameters.Add(new SqlParameter("@CodigoProvincia", ProvinceId));

                ApiHelper.OpenConnection(sqlCon);

                var concepts = new List<Concept>();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        concepts.Add(
                            new Concept{
                                Correlativo = (int) reader["Codigo"],
                                Descripcion = reader["Descripcion"].ToString()
                            }
                        );
                    }
                }
                
                ApiHelper.CloseConnection(sqlCon);

                if (concepts.Count == 0) {
                    response.codigo = 404;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontraron datos de ubigeo";
                } else {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = concepts;
                }                
            }
            catch (Exception e)
            {
                string DataEntrada = "{ Flag: " + Flag + ", IdDepartamento: " + DepartmentId + ", IdProvincia: " + ProvinceId + " }";
                saveControlError(Constant.IdApiBuscarConceptos, DataEntrada, "getLocation", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
    }
}