using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface IModuleModel
    {
        ResponseHeader GetUserModules(int ProfileId, int UserId);
    }
    public class ModuleModel : IModuleModel
    {
        private DataContext _context;
        GeneralModel generalModel;
        public ModuleModel(DataContext context)
        {
            _context = context;
        }
        public ResponseHeader GetUserModules(int _profileId, int _userId)
        {
            ResponseHeader response = new ResponseHeader();

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_accesos_modulos";
                cmd.Parameters.Add(new SqlParameter("@IdPerfil", _profileId));
                cmd.Parameters.Add(new SqlParameter("@IdUsuario", _userId));

                ApiHelper.OpenConnection(sqlCon);

                var modules = new List<Module>();
                var i = 0;
           
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Module module = new Module();
                        SubModule subModulo = new SubModule();

                        module.IdModulo = (int) reader["idModulo"];
                        module.NombreModulo = reader["nombre"].ToString();
                        module.UrlModulo = reader["Url"].ToString();

                        subModulo.IdSubmodulo = (int) reader["idSubmodulo"];
                        subModulo.NombreSubmodulo = reader["nombre_sub"].ToString();
                        subModulo.UrlSubmodulo = reader["Url_sub"].ToString();

                        if(i == 0)  {
                            modules.Add(module);
                            module.subModulos.Add(subModulo);                            
                            i++;
                        }else {
                            if (modules[i-1].IdModulo != module.IdModulo) {
                                modules.Add(module);
                                module.subModulos.Add(subModulo);                                
                                i++;
                            }else{
                                modules[i-1].subModulos.Add(subModulo);
                            }                            
                        }
                    }
                }

                if(modules.Count > 0) {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = modules;
                }
                else
                {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontraron modulos";
                }

                ApiHelper.CloseConnection(sqlCon);
            }
            catch (Exception e)
            {
                generalModel = new GeneralModel(_context);
                string DataEntrada = "{ IdPerfil: " + _profileId + ", IdUsuario: " + _userId + "}";
                generalModel.saveControlError(Constant.IdApiAccesosModulos, DataEntrada, "GetUserModules", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
    }
}