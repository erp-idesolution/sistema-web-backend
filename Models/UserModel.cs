using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface IUserModel
    {
        ResponseHeader ValidateLogin(LoginRequest loginRequest);
        User GetById(int id);
    }
    public class UserModel : IUserModel
    {
        private readonly AppSettings _appSettings;
        private DataContext _context;
        EnterpriseModel enterpriseModel;
        OfficeModel officeModel;
        GeneralModel generalModel;
        public UserModel(IOptions<AppSettings> appSettings, DataContext context)
        {
            _appSettings = appSettings.Value;
            _context = context;            
        }
        public ResponseHeader ValidateLogin(LoginRequest loginRequest)
        {
            ResponseHeader response = new ResponseHeader();
            enterpriseModel = new EnterpriseModel(_context);
            officeModel = new OfficeModel(_context);

            try
            {
                User user = _validateUserAccount(loginRequest.Cuenta);
                if (user.Id > 0) {
                    if (ApiHelper.CheckHash(loginRequest.Clave, user.Password, user.Salt)) {
                        Enterprise enterprise = enterpriseModel.getDataEnterprise();
                        List<Office> offices = officeModel.getUserOffices(user.Id);
                        Person person = _getDataUser(user.Id);

                        response.codigo = 200;
                        response.descripcion = "OK";
                        response.mensaje = "Transacción exitosa";
                        response.objeto = (new LoginResponse(){
                            cod_empresa = enterprise.IdEmpresa,
                            nombre_empresa = enterprise.NombreEmpresa,
                            url_logo = enterprise.LogoEmpresa,
                            cod_usuario = user.Id,
                            cod_perfil = user.IdPerfil,
                            sucursales = offices,
                            nombres = person.Nombres,
                            apellidoPaterno = person.ApellidoPaterno,
                            apellidoMaterno = person.ApellidoMaterno,
                            documento = user.NumeroDocumento,
                            cuenta = loginRequest.Cuenta,
                            id_token = generateJwtToken(user)
                        });
                    }
                    else
                    {
                        response.codigo = 403;
                        response.descripcion = "Error";
                        response.mensaje = "No hay acceso para este usuario";
                    }
                }
                else
                {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "Cuenta y/o clave no existe";
                }
            }
            catch (Exception e)
            {   
                generalModel = new GeneralModel(_context);
                generalModel.saveControlError(Constant.IdApiValidarCredenciales, loginRequest, "ValidateLogin", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public User GetById(int id)
        {
            return _getUsers().FirstOrDefault(x => x.Id == id);
        }
        // helper methods
        private string generateJwtToken(User user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        private List<User> _getUsers()
        {
            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_usuarios";

                ApiHelper.OpenConnection(sqlCon);

                var response = new List<User>();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        response.Add(
                            new User{
                                Id = (int) reader["idUsuario"],
                                FirstName = reader["nombre"].ToString(),
                                LastName = reader["apellidoPaterno"].ToString() + " " + reader["apellidoMaterno"].ToString(),
                                Username = reader["cuenta"].ToString()
                            }
                        );
                    }
                }
                ApiHelper.CloseConnection(sqlCon);
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private Person _getDataUser(int _userId)
        {
            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_datos_usuario";
                cmd.Parameters.Add(new SqlParameter("@codigo_usuario", _userId));

                ApiHelper.OpenConnection(sqlCon);

                var response = new Person();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        response.Nombres = reader["nombre"].ToString();
                        response.ApellidoPaterno = reader["apellidoPaterno"].ToString();
                        response.ApellidoMaterno = reader["apellidoMaterno"].ToString();
                    }
                }
                ApiHelper.CloseConnection(sqlCon);
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private User _validateUserAccount(string _userAccount)
        {
            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_validar_cuenta_usuario";
                cmd.Parameters.Add(new SqlParameter("@cuenta", _userAccount));

                ApiHelper.OpenConnection(sqlCon);

                var response = new User();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        response.Id = (int) reader["idUsuario"];
                        response.IdPerfil = (int) reader["idPerfil"];
                        response.NumeroDocumento = reader["documento"].ToString();
                        response.Password = reader["clave"].ToString();
                        response.Salt = reader["sal"].ToString();
                    }
                }
                ApiHelper.CloseConnection(sqlCon);
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}