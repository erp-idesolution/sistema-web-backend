using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface ISaleModel
    {
        ResponseHeader AddSale(AddUpdSaleRequest addUpdSaleRequest);
        ResponseHeader UpdateSale(AddUpdSaleRequest addUpdSaleRequest);
        ResponseHeader GetSales(ListSalesRequest listSalesRequest);
    }
    public class SaleModel : ISaleModel
    {
        private DataContext _context;
        GeneralModel generalModel;
        public SaleModel(DataContext context)
        {
            _context = context;
        }
        public ResponseHeader AddSale(AddUpdSaleRequest addUpdSaleRequest)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_registrar_venta";
                string xmlSale = ApiHelper.SerializeObject(addUpdSaleRequest);
                cmd.Parameters.Add(new SqlParameter("@Venta", xmlSale));

                var codigoRetorno = cmd.Parameters.Add("@Codigo", SqlDbType.Int); 
                codigoRetorno.Direction = ParameterDirection.Output;

                var mensajeRetorno = cmd.Parameters.Add("@Mensaje", SqlDbType.NVarChar, 500); 
                mensajeRetorno.Direction = ParameterDirection.Output;

                ApiHelper.OpenConnection(sqlCon);

                cmd.ExecuteNonQuery();
                int codigo = (int) codigoRetorno.Value;
                string mensaje = mensajeRetorno.Value.ToString();

                ApiHelper.CloseConnection(sqlCon);

                if(codigo > 0) {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = (new Response(){
                        Codigo = codigo,
                        Mensaje = mensaje
                    });
                } else {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "Ocurrió un error";
                    
                    generalModel.saveControlError(Constant.IdApiRegistrarVenta, addUpdSaleRequest, "AddSale", mensaje, "pa_registrar_venta", addUpdSaleRequest.CodigoSucursal, "0");
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiRegistrarVenta, addUpdSaleRequest, "AddSale", e.Message, e.StackTrace, addUpdSaleRequest.CodigoSucursal, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader UpdateSale(AddUpdSaleRequest addUpdSaleRequest)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_editar_venta";
                string xmlSale = ApiHelper.SerializeObject(addUpdSaleRequest);
                cmd.Parameters.Add(new SqlParameter("@Venta", xmlSale));

                var codigoRetorno = cmd.Parameters.Add("@Codigo", SqlDbType.Int); 
                codigoRetorno.Direction = ParameterDirection.Output;

                var mensajeRetorno = cmd.Parameters.Add("@Mensaje", SqlDbType.NVarChar, 500); 
                mensajeRetorno.Direction = ParameterDirection.Output;

                ApiHelper.OpenConnection(sqlCon);

                cmd.ExecuteNonQuery();
                int codigo = (int) codigoRetorno.Value;
                string mensaje = mensajeRetorno.Value.ToString();

                ApiHelper.CloseConnection(sqlCon);

                if(codigo > 0) {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = (new Response(){
                        Codigo = codigo,
                        Mensaje = mensaje
                    });
                } else {
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "Ocurrió un error";
                    
                    generalModel.saveControlError(Constant.IdApiEditarDetalleVenta, addUpdSaleRequest, "EditSaleDetail", mensaje, "pa_editar_venta", addUpdSaleRequest.CodigoSucursal, "0");
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiEditarDetalleVenta, addUpdSaleRequest, "EditSaleDetail", e.Message, e.StackTrace, addUpdSaleRequest.CodigoSucursal, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public ResponseHeader GetSales(ListSalesRequest listSalesRequest)
        {
            ResponseHeader response = new ResponseHeader();
            generalModel = new GeneralModel(_context);            

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_ventas";

                cmd.Parameters.Add(new SqlParameter("@FechaInicio", listSalesRequest.FechaInicio));
                cmd.Parameters.Add(new SqlParameter("@FechaFin", listSalesRequest.FechaFin));
                cmd.Parameters.Add(new SqlParameter("@DatosCliente", listSalesRequest.DatosCliente));
                cmd.Parameters.Add(new SqlParameter("@Estado", listSalesRequest.EstadoPago));
                cmd.Parameters.Add(new SqlParameter("@IdVenta", listSalesRequest.IdVenta));

                ApiHelper.OpenConnection(sqlCon);

                var sales = new List<ListSalesResponse>();
           
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        sales.Add(
                            new ListSalesResponse {
                                CodigoVenta = (int) reader["IdVenta"],
                                FechaCreacion = Convert.ToDateTime(reader["FechaCreacion"].ToString()).ToString("dd/MM/yyyy HH:mm:ss"),
                                DatosVendedor = reader["nombreUsuario"].ToString() + " " + reader["aPaternoUsuario"].ToString() + " " + reader["aMaternoUsuario"].ToString(),
                                DocumentoCliente = reader["documentoCliente"].ToString(),
                                DatosCliente = reader["nombreCliente"].ToString() + " " + reader["aPaternoCliente"].ToString() + " " + reader["aMaternoCliente"].ToString(),
                                Subtotal = (decimal) reader["Subtotal"],
                                MontoIGV = (decimal) reader["MontoIGV"],
                                TotalVenta = (decimal) reader["MontoTotal"],
                                EstadoPago = reader["EstadoPago"].ToString()
                            }
                        );
                    }
                }

                ApiHelper.CloseConnection(sqlCon);

                if(sales.Count == 0){
                    response.codigo = 400;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontraron ventas";
                } else {
                    // Add Sale Detail
                    foreach (var sale in sales) sale.DetalleVenta = GetSaleDetail(sale.CodigoVenta);

                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = sales;
                }
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiBuscarVentas, listSalesRequest, "GetSales", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
        public List<SaleDetailResponse> GetSaleDetail(int SaleId)
        {
            generalModel = new GeneralModel(_context);
            var details = new List<SaleDetailResponse>();

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_detalle_venta";
                cmd.Parameters.Add(new SqlParameter("@id_venta", SaleId));

                ApiHelper.OpenConnection(sqlCon);                
           
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    { 
                        details.Add(new SaleDetailResponse{
                            Item = (int) reader["Item"],
                            SkuProducto = reader["SkuProducto"].ToString(),
                            Descripcion = reader["DescripcionProducto"].ToString(),
                            Cantidad = (int) reader["CantidadProducto"],
                            Presentacion = reader["Presentacion"].ToString(),
                            Precio = (decimal) reader["PrecioProducto"],
                            Importe = (decimal) reader["ImporteProducto"]
                        });
                    }
                }

                ApiHelper.CloseConnection(sqlCon);
            }
            catch (Exception e)
            {
                generalModel.saveControlError(Constant.IdApiBuscarDetalleVenta, SaleId, "GetSaleDetail", e.Message, e.StackTrace, 0, "0");
            }
            return details;
        }
    }
}