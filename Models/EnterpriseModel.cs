using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public class EnterpriseModel
    {
        private DataContext _context;
        public EnterpriseModel(DataContext context)
        {
            _context = context;
        }
        public Enterprise getDataEnterprise()
        {
            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_datos_empresa";

                ApiHelper.OpenConnection(sqlCon);

                var response = new Enterprise();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        response.IdEmpresa = (int) reader["IdEmpresa"];
                        response.NombreEmpresa = reader["NombreEmpresa"].ToString();
                        response.LogoEmpresa = reader["LogoEmpresa"].ToString();
                    }
                }

                ApiHelper.CloseConnection(sqlCon);
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}