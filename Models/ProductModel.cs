using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Models
{
    public interface IProductModel
    {
        ResponseHeader getDataProduct(string Sku);
    }
    public class ProductModel : IProductModel
    {
        private DataContext _context;
        GeneralModel generalModel;
        public ProductModel(DataContext context)
        {
            _context = context;
        }
        public ResponseHeader getDataProduct(string Sku)
        {
            ResponseHeader response = new ResponseHeader();
            Product product = null;

            try
            {
                SqlConnection sqlCon = (SqlConnection)this._context.Database.GetDbConnection();
                SqlCommand cmd = sqlCon.CreateCommand();
                
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "pa_listar_datos_producto";
                cmd.Parameters.Add(new SqlParameter("@sku", Sku));

                ApiHelper.OpenConnection(sqlCon);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        product = new Product();
                        product.Descripcion = reader["DescripcionProducto"].ToString();
                        product.DescripcionPresentacion = reader["DescripcionPresentacion"].ToString();
                    }
                }
                
                ApiHelper.CloseConnection(sqlCon);

                if (product == null) {
                    response.codigo = 404;
                    response.descripcion = "Error";
                    response.mensaje = "No se encontró producto";
                } else {
                    response.codigo = 200;
                    response.descripcion = "OK";
                    response.mensaje = "Transacción exitosa";
                    response.objeto = product;
                }                
            }
            catch (Exception e)
            {
                generalModel = new GeneralModel(_context);
                generalModel.saveControlError(Constant.IdApiBuscarProducto, Sku, "getDataProduct", e.Message, e.StackTrace, 0, "0");
                
                response.codigo = 500;
                response.descripcion = "Error";
                response.mensaje = "Ocurrió un error no controlado";
            }
            return response;
        }
    }
}