namespace WebApi.Entities
{
    public class Presentation
    {
        public int IdPresentacion {get; set;}
        public string DescripcionPresentacion {get; set;}
    }
}