using System.Collections.Generic;
using WebApi.Entities;

namespace WebApi.Models
{
    public class LoginResponse
    {
        public int cod_empresa {get; set;}
        public string nombre_empresa {get; set;}
        public string url_logo {get; set;}
        public int cod_usuario {get; set;}
        public int cod_perfil {get; set;}
        public List<Office> sucursales {get; set;}
        public string nombres {get; set;}
        public string apellidoPaterno {get; set;}
        public string apellidoMaterno {get; set;}
        public string documento {get; set;}
        public string cuenta {get; set;}
        public string id_token {get; set;}
    }
}