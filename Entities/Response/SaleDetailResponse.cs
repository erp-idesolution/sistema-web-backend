namespace WebApi.Models
{
    public class SaleDetailResponse
    {
        public int Item {get; set;}
        public string SkuProducto {get; set;}
        public string Descripcion {get; set;}
        public int Cantidad {get; set;}
        public string Presentacion {get; set;}
        public decimal Precio {get; set;}
        public decimal Importe {get; set;}        
    }
}