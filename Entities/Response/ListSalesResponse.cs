using System.Collections.Generic;

namespace WebApi.Models
{
    public class ListSalesResponse
    {
        public int CodigoVenta {get; set;}
        public string FechaCreacion {get; set;}
        public string DatosVendedor {get; set;}
        public string DocumentoCliente {get; set;}
        public string DatosCliente {get; set;}
        public decimal Subtotal {get; set;}
        public decimal MontoIGV {get; set;}
        public decimal TotalVenta {get; set;}
        public string EstadoPago {get; set;}
        public List<SaleDetailResponse> DetalleVenta {get; set;}
        public ListSalesResponse(){
            DetalleVenta = new List<SaleDetailResponse>();
        }
    }
}