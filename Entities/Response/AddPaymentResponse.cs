namespace WebApi.Models
{
    public class AddPaymentResponse
    {
        public int CodigoPago {get; set;}
        public string Mensaje {get; set;}
        public string SeriePago {get; set;}
        public int CorrelativoPago {get; set;}
    }
}