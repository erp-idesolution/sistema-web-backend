namespace WebApi.Models
{
    public class AddSaleResponse
    {
        public int CodigoVenta {get; set;}
        public string Mensaje {get; set;}
    }
}