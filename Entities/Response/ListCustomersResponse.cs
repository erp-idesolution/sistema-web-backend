namespace WebApi.Models
{
    public class ListCustomersResponse
    {
        public int CodigoCliente {get; set;}
        public int TipoPersona {get; set;}
        public string DescripcionTipoPersona {get; set;}
        public string Nombres {get; set;}
        public int TipoDocumento {get; set;}
        public string NumeroDocumento {get; set;}
        public string FechaNacimiento {get; set;}
        public string NumeroContacto {get; set;}
        public string Correo {get; set;}
        public string Direccion {get; set;}
        public string Departamento {get; set;}
        public string Provincia {get; set;}
        public string Distrito {get; set;}
    }
}