using System.Collections.Generic;

namespace WebApi.Entities
{
    public class Module
    {
        public int IdModulo {get; set;}        
        public string NombreModulo {get; set;}
        public string UrlModulo {get; set;}
        public List<SubModule> subModulos {get; set;}
        public Module(){
            subModulos = new List<SubModule>();
        }
    }
    public class SubModule
    {
        public int IdSubmodulo {get; set;}
        public string NombreSubmodulo {get; set;}
        public string UrlSubmodulo {get; set;}
    }
}