namespace WebApi.Entities
{
    public class Product
    {
        public string SKU {get; set;}
        public string Descripcion {get; set;}
        public string DescripcionPresentacion {get; set;}
    }
}