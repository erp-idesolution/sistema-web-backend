using System.Text.Json.Serialization;

namespace WebApi.Entities
{
    public class User
    {
        public int Id { get; set; }
        public int IdPerfil { get; set; }
        public string NumeroDocumento { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Salt { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
    }
}