namespace WebApi.Entities
{
    public class SaleDetail
    {
        public int Item {get; set;}
        public decimal Precio {get; set;}
        public int Cantidad {get; set;}
        public decimal Importe {get; set;}
        public string Presentacion {get; set;}
        public Product Producto {get; set;}
    }
}