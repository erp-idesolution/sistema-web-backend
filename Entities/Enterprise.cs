namespace WebApi.Entities
{
    public class Enterprise
    {
        public int IdEmpresa {get; set;}
        public string NombreEmpresa {get; set;}
        public string LogoEmpresa {get; set;}
    }
}