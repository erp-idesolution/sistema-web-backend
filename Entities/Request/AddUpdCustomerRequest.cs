namespace WebApi.Entities
{
    public class AddUpdCustomerRequest
    {       
        public int CodigoCliente { get; set; }
        public int TipoPersona { get; set; }
        public string Nombres { get; set; }
        public string ApellidoPaterno {get; set;}
        public string ApellidoMaterno {get; set;}
        public int TipoDocumento {get; set;}
        public string NumeroDocumento {get; set;}
        public string FechaNacimiento {get; set;}
        public string NumeroContacto {get; set;}
        public string Correo {get; set;}
        public string Direccion {get; set;}
        public int CodigoDepartamento { get; set; }
        public int CodigoProvincia { get; set; }
        public int CodigoDistrito { get; set; }
    }
}