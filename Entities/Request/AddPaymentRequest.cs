namespace WebApi.Entities
{
    public class AddPaymentRequest
    {        
        public int TipoComprobante { get; set; }
        public string FechaPago { get; set; }
        public int MedioPago { get; set; }
        public string DocumentoCliente { get; set; }
        public decimal MontoImporte { get; set; }      
        public decimal MontoEfectivo { get; set; }
        public decimal MontoVuelto { get; set; }
        public int CodigoBanco { get; set; }
        public string NumeroOperacion { get; set; }
        public int CodigoSucursal { get; set; }
        public int CodigoUsuario { get; set; }
        public int CodigoVenta { get; set; }
    }
}