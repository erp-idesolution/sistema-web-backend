namespace WebApi.Entities
{
    public class ListCustomersRequest
    {
        public string NumeroDocumento {get; set;}
        public string DatosCliente {get; set;}
        public int TipoPersona {get; set;}
    }
}