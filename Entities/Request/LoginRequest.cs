namespace WebApi.Entities
{
    public class LoginRequest
    {
        public string Cuenta { get; set; }
        public string Clave { get; set; }
    }
}