namespace WebApi.Entities
{
    public class ListSalesRequest
    {
        public string FechaInicio {get; set;}
        public string FechaFin {get; set;}
        public string DatosCliente {get; set;}
        public int EstadoPago {get; set;}
        public int IdVenta {get; set;}
    }
}