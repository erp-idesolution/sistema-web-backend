using System.Collections.Generic;

namespace WebApi.Entities
{
    public class AddUpdSaleRequest
    {       
        public int CodigoVenta { get; set; }
        public string FechaCreacion { get; set; }
        public int CodigoSucursal { get; set; }
        public int CodigoUsuario { get; set; }
        public int NumeroArticulos { get; set; }
        public decimal Subtotal { get; set; }
        public decimal MontoTotal { get; set; }
        public decimal MontoIGV { get; set; }
        public Person Cliente { get; set; }
        public List<SaleDetail> DetalleVenta { get; set; }
    }
}