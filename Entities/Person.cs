namespace WebApi.Entities
{
    public class Person
    {
        public int IdUsuario {get; set;}
        public int TipoDocumento {get; set;}
        public string NumeroDocumento {get; set;}
        public string Nombres {get; set;}
        public string ApellidoPaterno {get; set;}
        public string ApellidoMaterno {get; set;}
    }
}