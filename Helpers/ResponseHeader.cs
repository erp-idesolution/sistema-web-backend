namespace WebApi.Helpers
{
    public class ResponseHeader
    {
        public int codigo { get; set; }
        public string  descripcion  { get; set; }
        public string  mensaje  { get; set;}
        public object objeto{get; set;}
    }
}