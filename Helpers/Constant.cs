namespace WebApi.Helpers
{
    public static class Constant
    {
        public const int IdApiValidarCredenciales = 1;
        public const int IdApiAccesosModulos = 2;
        public const int IdApiRegistrarVenta = 3;
        public const int IdApiBuscarCliente = 4;
        public const int IdApiBuscarProducto = 5;
        public const int IdApiBuscarPresentacionesProducto = 6;
        public const int IdApiBuscarParametros = 7;
        public const int IdApiBuscarVentas = 8;
        public const int IdApiBuscarDetalleVenta = 9;
        public const int IdApiRegistrarPago = 10;
        public const int IdApiEditarDetalleVenta = 11;
        public const int IdApiBuscarConceptos = 12;
        public const int IdApiBuscarClientes = 13;
        public const int IdApiRegistrarCliente = 14;
        public const int IdApiEditarCliente = 15;
        public const int IdApiEliminarrCliente = 16;
    }
}