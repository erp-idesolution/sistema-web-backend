using System;
using System.Data;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Data.SqlClient;

namespace WebApi.Helpers
{
    public class ApiHelper
    {
        public static void OpenConnection(SqlConnection Connection)
        {
            if (Connection.State == ConnectionState.Closed) Connection.Open();
        }
        public static void CloseConnection(SqlConnection Connection)
        {
            if (Connection.State == ConnectionState.Open) Connection.Close();
        }
        public static HashedPassword Hash(string password)
        {
            byte[] salt = new byte[128 / 8];

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8
            )
            );

            return new HashedPassword() { Password = hashed, Salt = Convert.ToBase64String(salt) };
        }
        public static bool CheckHash(string attemptedPassword, string hash, string salt)
        {
            string 
                hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: attemptedPassword,
                salt: Convert.FromBase64String(salt),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
                
            return hash == hashed;
        }
        public static byte[] GetHash(string password, string salt)
        {
            byte[] unhashedBytes = Encoding.Unicode.GetBytes(string.Concat(salt, password));
            SHA256Managed sha256 = new SHA256Managed();
            byte[] hashedBytes = sha256.ComputeHash(unhashedBytes);

            return hashedBytes;
        }
        public static string SerializeObject<T> (T obj)
        {
            string xml = "";
            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww, settings))
                {
                    mySerializer.Serialize(writer, obj);
                    xml = sww.ToString();
                }
            }
            return xml;
        }
    }
    public class HashedPassword
    {
        public string Password { get; set;}
        public string Salt { get; set; }
    }
}